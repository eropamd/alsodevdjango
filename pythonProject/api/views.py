from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth.models import User
from rest_framework_simplejwt.views import TokenObtainPairView
from django.contrib.auth import authenticate, login, logout
from rest_framework.permissions import IsAuthenticated
from project.models import ProjectModel, TaskModel
from .serializers import ProjectSerializer, TaskSerializer
from django.shortcuts import get_object_or_404
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi


class LoginView(TokenObtainPairView):
    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'username': openapi.Schema(type=openapi.TYPE_STRING),
                'password': openapi.Schema(type=openapi.TYPE_STRING),
            },
            required=['username', 'password'],
        ),
        responses={
            200: openapi.Response(
                description='Get access_token',
                schema=openapi.Schema(
                    type=openapi.TYPE_OBJECT,
                    properties={
                        'access_token': openapi.Schema(type=openapi.TYPE_STRING),
                        'refresh_token': openapi.Schema(type=openapi.TYPE_STRING),
                    },
                ),
            ),
            401: openapi.Response(description='Invalid credentials'),
        },
    )
    def post(self, request, *args, **kwargs):
        username = request.data.get('username')
        password = request.data.get('password')
        if username is None or password is None:
            return Response({'error': 'Please provide both username and password'},
                            status=status.HTTP_400_BAD_REQUEST)
        user = authenticate(username=username, password=password)
        if user is None:
            return Response({'error': 'Invalid credentials'},
                            status=status.HTTP_401_UNAUTHORIZED)
        refresh = RefreshToken.for_user(user)
        access_token = refresh.access_token
        response_data = {
            'refresh_token': str(refresh),
            'access_token': str(access_token),
            'expires_in': access_token.lifetime.total_seconds()  # Укажите количество секунд вместо timedelta
        }
        return Response(response_data, status=status.HTTP_200_OK)

class UserView(APIView):
    permission_classes = [IsAuthenticated]
    def get(self, request):
        user = request.user
        return Response({'user.id':user.id,'user.username':user.username})
    def patch(self, request):
        user = request.user
        new_username = request.data.get("new_username")
        new_email = request.data.get("new_email")
        user.username = new_username if new_username else user.username
        user.email = new_email if new_email else user.email
        user.save()
        return Response({"user.id": user.id, "user.username": user.username, "user.email": user.email})


class RegisterView(APIView):
    def post(self, request):
        try:
            username = request.data.get('username')
            password = request.data.get('password')

            if User.objects.filter(username=username).exists():
                return Response({'detail': 'User with this username already exists'}, status=status.HTTP_400_BAD_REQUEST)
            user = User.objects.create_user(username=username, password=password)
            refresh = RefreshToken.for_user(user)
            return Response({'access_token': str(refresh.access_token), 'refresh_token': str(refresh)}, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response({"detail": "Произошла ошибка при регистрации"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class LogoutView(APIView):
    permission_classes = [IsAuthenticated]
    def post(self, request):
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist_after = None
            return Response({"detail": "Успешный выход из системы и блокировка токена"}, status=status.HTTP_200_OK)
        except KeyError:
            return Response({"detail": "Отсутствует 'refresh_token' в запросе"}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            # Обработка других исключений, если необходимо
            return Response({"detail": "Произошла ошибка при блокировке токена"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class ProjectView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        user = request.user
        user_instance = User.objects.get(id=user.id)
        projects = ProjectModel.objects.filter(users=user_instance)
        serializer = ProjectSerializer(projects, many=True)
        return Response(serializer.data)
    def put(self, request):
        user = request.user
        # Получение данных из запроса
        project_name = request.data.get("project_name")
        # Создание нового проекта
        project = ProjectModel.objects.create(name=project_name)
        project.users.add(user)
        serializer = ProjectSerializer(project)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ProjectDetailView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, project_id):
        user = request.user
        project = get_object_or_404(ProjectModel, id=project_id, users=user)
        serializer = ProjectSerializer(project)
        return Response(serializer.data)

    def put(self, request, project_id):
        user = request.user
        project = get_object_or_404(ProjectModel, id=project_id, users=user)
        new_project_name = request.data.get("new_project_name")
        project.name = new_project_name if new_project_name else project.name
        project.save()
        serializer = ProjectSerializer(project)
        return Response(serializer.data)
    def delete(self, request, project_id):
        user = request.user
        project = get_object_or_404(ProjectModel, id=project_id, users=user)
        project.delete()
        return Response({"detail": "Проект успешно удален"}, status=status.HTTP_204_NO_CONTENT)

class TaskView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        tasks_for_user = TaskModel.objects.filter(users=request.user)
        serializer = TaskSerializer(tasks_for_user, many=True)
        return Response(serializer.data)
    def post(self, request):
        user = request.user
        name = request.data.get("name")
        project_id = request.data.get("project_id")
        project = get_object_or_404(ProjectModel, id=project_id, users=user)
        task = TaskModel.objects.create(name=name,project=project)
        task.users.add(user)
        serializer = TaskSerializer(task)
        return Response(serializer.data, status=status.HTTP_201_CREATED)