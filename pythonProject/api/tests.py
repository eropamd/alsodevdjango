from django.contrib.auth.models import User
from rest_framework.test import APITestCase
from rest_framework import status

class YourApiTests(APITestCase):
    def setUp(self):
        # Создаем тестового пользователя
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        # Аутентифицируем пользователя и получаем токены
        response = self.client.post('/api/auth/login/', {'username': 'testuser', 'password': 'testpassword'})
        self.access_token = response.data['access_token']
        self.refresh_token = response.data['refresh_token']
        # Устанавливаем токен в заголовок авторизации
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.access_token}')

    def test_user_view(self):
        response = self.client.get('/api/auth/user/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_update_view(self):
        response = self.client.patch('/api/auth/user/', {'new_username': 'new_testuser'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['user.username'], 'new_testuser')

    def test_register_view(self):
        response = self.client.post('/api/auth/register/', {'username': 'newuser', 'password': 'newpassword'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_logout_view(self):
        response = self.client.post('/api/auth/logout/', {'refresh_token': self.refresh_token})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_project_view(self):
        response = self.client.get('/api/projects/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_project_create_view(self):
        response = self.client.put('/api/projects/', {'project_name': 'Test Project'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_task_view(self):
        response = self.client.get('/api/tasks/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
