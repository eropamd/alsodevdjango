from django.conf import settings

from django.urls import path
from .views import UserView, LoginView, LogoutView, RegisterView, ProjectView, ProjectDetailView, TaskView

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi



schema_view = get_schema_view(
    openapi.Info(
        title="Your API",
        default_version='v1',
        description="Your API description",
        terms_of_service="https://www.yourapp.com/terms/",
        contact=openapi.Contact(email="contact@yourapp.com"),
        license=openapi.License(name="Your License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('auth/login/', LoginView.as_view(), name='api_token_obtain_pair'),
    path('auth/user/', UserView.as_view(), name='api_private_view'),
    path('auth/logout/', LogoutView.as_view(), name='api_logout_view'),
    path('auth/register/', RegisterView.as_view(), name='api_register_view'),
    path('projects/', ProjectView.as_view(), name='api_project_view'),
    path('projects/<int:project_id>/', ProjectDetailView.as_view(), name='api_project_detail'),
    path('tasks/', TaskView.as_view(), name='api_tasks'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]