# serializers.py
from rest_framework import serializers
from project.models import ProjectModel, TaskModel

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectModel
        fields = ['id', 'name', 'description']

class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskModel
        fields = ['id', 'project', 'name', 'description', 'deadline', 'status']