# Install
Запускаем докер
`docker-compose up --b`

Заходим в докер
`docker-compose exec web bash`

В докере мигрируем таблицы
`python manage.py migrate`

Создаем нового пользователя
`python manage.py createsuperuser`

#Run
Запускаем докер  (если был установлен)
`docker-compose up`

#test

Run in docker
` python manage.py test api.tests`
