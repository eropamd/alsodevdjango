from django import forms
from .models import ProjectModel,TaskModel
from django.contrib.auth.models import User

class ProjectForm(forms.ModelForm):
    users = forms.ModelMultipleChoiceField(queryset=User.objects.all(), widget=forms.CheckboxSelectMultiple)
    class Meta:
        model = ProjectModel
        fields = ['name', 'description','startline','deadline','users']

class ProjectUpdateForm(forms.ModelForm):
    users = forms.ModelMultipleChoiceField(queryset=User.objects.all(), widget=forms.CheckboxSelectMultiple)
    class Meta:
        model = ProjectModel
        fields = ['name', 'description','startline','deadline','users']


class TaskForm(forms.ModelForm):
    users = forms.ModelMultipleChoiceField(queryset=User.objects.all(), widget=forms.CheckboxSelectMultiple)
    class Meta:
        model = TaskModel
        fields = ['name','description','deadline','status', 'users','project', 'users']

class TaskUpdateForm(forms.ModelForm):
    users = forms.ModelMultipleChoiceField(queryset=User.objects.all(), widget=forms.CheckboxSelectMultiple)
    class Meta:
        model = TaskModel
        fields = ['id','name','description','deadline','status', 'users','project', 'users']