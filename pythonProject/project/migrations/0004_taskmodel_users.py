# Generated by Django 4.2 on 2024-02-20 19:24

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('project', '0003_alter_taskmodel_deadline'),
    ]

    operations = [
        migrations.AddField(
            model_name='taskmodel',
            name='users',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
    ]
