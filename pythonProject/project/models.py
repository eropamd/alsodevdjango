from django.db import models
from django.contrib.auth.models import User


class ProjectModel(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    users = models.ManyToManyField(User)
    description = models.TextField(blank=True, null=True)
    startline = models.DateTimeField(blank=True, null=True)
    deadline = models.DateTimeField(blank=True, null=True)
    def __str__(self):
        return self.name
    class Meta:
        db_table = 'projects'

class TaskModel(models.Model):
    STATUS_CHOICES = [
        ('in_process', 'In Process'),
        ('completed', 'Completed'),
        ('postponed', 'Postponed'),
    ]
    id = models.AutoField(primary_key=True)
    project = models.ForeignKey(ProjectModel, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    deadline = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='in_process')
    users = models.ManyToManyField(User)
    def __str__(self):
        return self.name
    class Meta:
        db_table = 'tasks'

