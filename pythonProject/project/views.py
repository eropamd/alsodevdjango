from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from .forms import ProjectForm,TaskForm,ProjectUpdateForm,TaskUpdateForm
from .models import ProjectModel,TaskModel
from timetracking.models import TimeEntry


# Create your views here.
@login_required(login_url='login')
def projectnew_view(request):
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save()  # Save the project instance and related users
            return redirect('/')
    user = request.user
    form = ProjectForm()

    usersquery = User.objects.all()
    return render(request, 'project_new.html', {'user': user,'form' :form,'usersquery': usersquery})

@login_required(login_url='login')
def project_view(request, id:int):
    user = request.user
    project_data = get_object_or_404(ProjectModel, id=id)
    tasks_for_project = TaskModel.objects.filter(project_id=id)
    # Добавим статус таймера для каждой задачи
    task_statuses = {}
    timer_is_running = False
    for task in tasks_for_project:
        timer_is_running = TimeEntry.objects.filter(task=task, user=user, end_time__isnull=True).exists()
        task_statuses[task.id] = {'timer_is_running': timer_is_running}

    return render(request, 'projects.html', {'user': user, 'project_data': project_data,
                                             'task_statuses':task_statuses,
                                             'timer_is_running':timer_is_running,
                                             'tasks_for_project':tasks_for_project})

@login_required(login_url='login')
def project_edit(request, id:int):
    project_data = get_object_or_404(ProjectModel, id=id)
    usersquery = User.objects.all()
    if request.method == 'POST':
        form = ProjectUpdateForm(request.POST, instance=project_data)
        if form.is_valid():
            project = form.save()  # Save the project instance and related users
            return redirect('/')
    users_in_project = project_data.users.all()
    return render(request, 'project_edit.html', {'project_data': project_data,
                                                 'users_in_project':users_in_project,
                                                 'usersquery': usersquery})
@login_required(login_url='login')
def project_delete(request, id:int):
    project = get_object_or_404(ProjectModel, id=id)
    project.delete()
    return redirect('/')

@login_required(login_url='login')
def tasknew_view(request,id):
    if request.method == 'POST':
        form = TaskForm(request.POST)
        print(request.POST)
        if form.is_valid():
            task = form.save()  # Save the project instance and related users
            return redirect('/projects/'+str(id)+'/')
    user = request.user
    usersquery = User.objects.all()
    project_data = get_object_or_404(ProjectModel, id=id)
    return render(request, 'task_new.html', {'user': user,'project_data': project_data,'usersquery':usersquery})

@login_required(login_url='login')
def taskedit_view(request,id):
    user = request.user
    task_edit = get_object_or_404(TaskModel, id=id)
    if request.method == 'POST':
        form = TaskUpdateForm(request.POST, instance=task_edit)
        if form.is_valid():
            project = form.save()
            return redirect('/projects/'+str(task_edit.project_id)+'/')
    users_in_task = task_edit.users.all()
    task_entries = TimeEntry.objects.filter(task_id=id)
    usersquery = User.objects.all()

    return render(request, 'task_edit.html', {'user': user,
                                              'users_in_task':users_in_task,
                                              'task_entries':task_entries,
                                              'task_edit': task_edit,
                                              'usersquery':usersquery})

@login_required(login_url='login')
def taskdelete_view(request, id:int):
    task = get_object_or_404(TaskModel, id=id)
    project_id=task.project_id
    task.delete()
    return redirect('/projects/'+str(project_id)+'/')