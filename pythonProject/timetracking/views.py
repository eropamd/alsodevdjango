from django.shortcuts import render, redirect,get_object_or_404
from .models import TimeEntry
from .forms import TimeEntryForm
from project.models import TaskModel
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from datetime import datetime, timedelta


def time_start_task(request, id):
    task = get_object_or_404(TaskModel, id=id)
    user = request.user
    if not TimeEntry.objects.filter(task=task, user=user, end_time__isnull=True).exists():
        TimeEntry.objects.create(task=task, user=user)
    else:
        time_entry = TimeEntry.objects.filter(task=task, user=user, end_time__isnull=True).first()
        time_entry.end_time = timezone.now()
        time_entry.duration = time_entry.end_time - time_entry.start_time
        time_entry.save()
        secTotal = time_entry.duration.total_seconds()
        time_entry.duration_n = int(secTotal // 3600)
        time_entry.duration_m = int(secTotal // 3600) % 60
        time_entry.save()
    return redirect('/projects/' + str(task.project_id) + '/')


@login_required(login_url='login')
def time_edit(request, id):
    timeTask = get_object_or_404(TimeEntry, id=id)
    if request.method == 'POST':
        dataPost = request.POST
        timeEnd = timeTask.start_time + timedelta(hours=int(dataPost['hour']), minutes=int(dataPost['minutes']))
        timeTask.end_time = timeEnd
        timeTask.duration = timeTask.end_time - timeTask.start_time
        timeTask.save();
    return redirect('/task/'+ str(timeTask.task_id) +'/edit/')

@login_required(login_url='login')
def time_new(request, id):
    dataTask = get_object_or_404(TaskModel, id=id)
    user = request.user
    if request.method == 'POST':
        dataPost = request.POST
        new_entry = TimeEntry.objects.create(
            user=user,
            task=dataTask,
            start_time=timezone.now(),
            end_time=timezone.now() + timezone.timedelta(hours=int(dataPost['hour']), minutes=int(dataPost['minutes'])),
        )
        new_entry.duration = new_entry.end_time - new_entry.start_time
        new_entry.save()
    return redirect('/task/'+ str(dataTask.id) +'/edit/')


