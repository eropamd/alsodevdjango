from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from project.models import TaskModel

class TimeEntry(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    task = models.ForeignKey(TaskModel, on_delete=models.CASCADE)
    start_time = models.DateTimeField(default=timezone.now)
    end_time = models.DateTimeField(null=True, blank=True)
    duration = models.DurationField(null=True, blank=True)


    def is_running(self):
        return self.end_time is None

    def duration_in_hour(self):
        return int(self.duration.total_seconds() // 3600)

    def duration_in_minutes(self):
        return int((self.duration.total_seconds() // 60) % 60)


