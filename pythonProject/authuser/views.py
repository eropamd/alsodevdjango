from django.shortcuts import render,redirect
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from .forms import LoginForm,RegistrationForm,ChangePassowrdForm
from project.models import ProjectModel
from django.contrib.auth.models import User

def login_view(request):
    user = request.user
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('success')  # Redirect to a success page
            else:
                return render(request, "login.html",{ "errors": "Invalid login credentials",})
    return render(request, "login.html",{'user': user})

@login_required(login_url='login')
def success_view(request):
    user = request.user
    return render(request, "index.html",{'user': user})

@login_required(login_url='login')
def home_view(request):
    user = request.user
    user_instance = User.objects.get(id=user.id)
    projects_for_user = ProjectModel.objects.filter(users=user_instance)
    return render(request, 'index.html',{'user': user,'projects_for_user':projects_for_user})

def logout_view(request):
    logout(request)
    return redirect('home')
@login_required(login_url='login')
def changepassowrd_view(request):
    if request.method == 'POST':
        form = ChangePassowrdForm(request.POST)
        if form.is_valid():
            password = form.cleaned_data['password']
            user = request.user
            userData = User.objects.get(id=user.id)
            userData.set_password(password)
            userData.save()
            return render(request, 'changepassword.html',{'statusok':"change password"});
    return render(request,'changepassword.html')

def register_view(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        print(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('home')
        else:
            return render(request, "signup.html", {"errors": "Не получилось зарегестрироваться"})
    return render(request, 'signup.html')