
from django.contrib import admin
from django.urls import path,include
from authuser.views import login_view, home_view, logout_view, register_view, changepassowrd_view
from project.views import projectnew_view,project_view,tasknew_view,project_edit,project_delete,taskedit_view,taskdelete_view
from timetracking.views import time_start_task,time_edit,time_new



urlpatterns = [
    path('admin/', admin.site.urls),

    path('changepassword/',changepassowrd_view,name='chanepassword'),
    path('', home_view, name='home'),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('success/', home_view, name="success"),
    path('signup/',register_view,name="signup"),

    path('project_new/',projectnew_view,name="project_new"),
    path('projects/<int:id>/',project_view,name="project"),
    path('projects/<int:id>/edit/',project_edit,name="projectedit"),
    path('projects/<int:id>/delete/',project_delete,name="projectdelete"),
    path('projects/<int:id>/tasknew/',tasknew_view,name="tasknew_view"),

    path('task/<int:id>/edit/',taskedit_view,name="task_edit"),
    path('task/<int:id>/delete/',taskdelete_view,name="tasknew_delete"),

    path('timer/<int:id>/start/',time_start_task,name="start_timer"),
    path('timer/<int:id>/edit/',time_edit,name="start_timer"),
    path('timer/<int:id>/new/',time_new,name="start_timer"),

   ## path('api/auth/login/', LoginView.as_view(), name='apilogin'),
    path('api/',include('api.urls')),
]

